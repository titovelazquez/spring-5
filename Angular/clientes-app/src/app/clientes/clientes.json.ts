import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
    new Cliente(1, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(2, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(3, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(4, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(5, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(6, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(7, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(8, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(9, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(10, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(11, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
    new Cliente(12, 'Tito Adrian', 'Velazquez Ballesteros', 'titoadrianvelazquez@hotmail.com', '2020-12-03'),
];
